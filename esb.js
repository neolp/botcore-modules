const { core, Node } = require('botcorejs')

class ESB extends Node {
  constructor(cfg) {
    super(cfg, core)
    this.connector = core.getConnector(cfg.addr)
    this.addVar('supplyvoltage', { type: 'number', label: 'input volatge', units: 'V', min: 9, max: 16 })
    this.populate(['addr', 'setport', 'getport', 'getports', 'hv'])
    console.log('esb start')
  }
  start() {
    this.set('state', 'start')
    let intreval = 1000
    this.poll = setInterval(async () => {
      try {
        if (this.cfg.fake) {
        } else {
          let st = this.get('state')
          if (st === 'start') {
            let status = await this.connector.call('stat', [1]) // reset status
            if (Array.isArray(status))
              this.startreason = status[1]
            //await this.connector.call('hv', [0, 1])
            //await this.connector.call('hv', [5, 1])
            // let cfg = await this.config()
            // if (cfg)
            this.set('state', 'ready')
          } else {
            // await this.adc()
            console.log('esb')
            let status = await this.connector.call('stat', [])
            let gets = await this.connector.call('get', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
            //            this.processstatus(status)
            console.log('esb', status)
            this.set('supplyvoltage', status[0] / 1000)
            if (status[1] !== 0) this.set('state', 'start')
            console.log('esb', gets)
          }
        }
      } catch (err) {
        this.set('state', 'error')
        this.set('statedesc', err.message)
      }
    }, intreval)
  }

  addr() {
    return this.connector.call('addr', []).then((data) => {
      console.log('addr', data)
      return data
    })
  }
  setport(port, val) {
    return this.connector.call('set', [port, val]).then((data) => {
      console.log('set', data)
      return data[0]
    })
  }
  getport(port) {
    return this.connector.call('get', [port]).then((data) => {
      console.log('get', data)
      return data[0]
    })
  }
  getports(portsarr) { // get values if several ports listed in array
    return this.connector.call('get', portsarr).then((data) => {
      console.log('get', data)
      return data
    })
  }
  hv(port, val) {
    return this.connector.call('hv', [port, val ? 1 : 0]).then((data) => {
      console.log('hv', data)
      return data
    })
  }
}
module.exports = ESB