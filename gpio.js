const Gpio = require('pigpio').Gpio;
const { core, Node } = require('botcorejs')

class Pin extends Node {
  constructor(cfg) {
    super(cfg, core)
    this.pin = new Gpio(cfg.pin, { mode: Gpio.OUTPUT });
    this.addVar('state', { type: 'number', default:0, label: 'pin' })
    if (cfg.ON) {
      this.set('state',1)
      this.ON()
    }

    this.addVar('state', { type: 'number', label: 'pin' })
    this.subscribe('state', (name, val) => {
      this.SET(val ? 1 : 0)
    })
  }

  start() {
  }

  ON() {
    this.pin.digitalWrite(1)
  }
  OFF() {
    this.pin.digitalWrite(0)
  }
  SET(value) {
    this.pin.digitalWrite(value)
  }
  GET() {
    return this.pin.digitalRead()
  }
}
module.exports = Pin