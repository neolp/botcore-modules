const fs = require("fs")
const { core, Node } = require('botcorejs')

class Joystick extends Node {
  constructor(cfg) {
    super(cfg, core)
    this.fileDescriptor = null;
    this.but = []
    this.pos = []
    this.addVar('stick', { default: { x: 0, y: 0 }, type: 'number' })
    this.addVar('buttons', { default: [], type: '[number]' })
    this.x = cfg.x
    this.y = cfg.y
    process.on('SIGTERM', () => {
      console.error('SIGTERM', 'joy')
      // this.close()
      this.stop()
      console.error('SIGTERM', 'joy2')
    })
    process.on('SIGINT', () => {
      console.error('SIGINT', 'joy')
      // this.close()
      this.stop()
      console.error('SIGINT', 'joy2')
      //process.abort()
      setTimeout(() => { process.abort(); console.error('EXIT'); }, 100)
    })
  }


  start() {
    // var self = this;

    // fs.open(this.cfg.device, "r", (err, fd) => {
    //   if (err) {
    //     // return cb(new Error(util.format(
    //     //   "Failed opening device '%s' (%s)",
    //     //   device, err.message
    //     // )));
    //   } else {

    //     self.fileDescriptor = fd;
    //     // cb();
    //     this.read()
    //   }
    // });

    this.stream = fs.createReadStream(this.cfg.device);
    this.stream.on('data', (buffer) => {
      this.handle(buffer);
    })
    this.stream.on('error', (err) => {
      console.log('error', err)
    })
  }

  stop() {
    this.stream.close() // This may not close the stream.
    // Artificially marking end-of-stream, as if the underlying resource had
    // indicated end-of-file by itself, allows the stream to close.
    // This does not cancel pending read operations, and if there is such an
    // operation, the process may still not be able to exit successfully
    // until it finishes.
    this.stream.push(null)
    this.stream.read(0)
    this.stream.destroy()
    console.log('shold be destroyed')
  }


  read() {
    var self = this;

    if (self.fileDescriptor) {
      var buffer = Buffer.allocUnsafe(8)

      fs.read(self.fileDescriptor, buffer, 0, buffer.length, null, (err, bytesRead) => {
        //console.log('J')
        if (err) {
          self.emit('readerror', err)
        } else if (bytesRead > 0) {
          self.handle(buffer);
          self.read();
        }

      });
    }
  };

  close() {
    this.removeAllListeners();
    fs.closeSync(this.fileDescriptor);
    this.fileDescriptor = null;
  };

  handle(bytes) {
    const byte5 = bytes.readUInt8(4)
    const position = bytes.readInt16LE(4)
    const byte7 = bytes.readUInt8(6) // button ==1 or stick ==2 (0x80 for initial value)
    const byte8 = bytes.readUInt8(7) // button or stick number

    if ((byte7 & 0x7F) === 1) {
      this.but[byte8] = byte5
      this.emit('button', byte8, byte5)
      this.set('buttons', this.but)
      //console.log('J', this.but)
    } else if ((byte7 & 0x7F) === 2) {
      this.pos[byte8] = position
      this.emit('stick', byte8, position)
      //console.log('stick', this.pos)
      if (this.x === byte8) {
        let cur = this.get('stick')
        this.set('stick', { x: position / 32767, y: cur.y })
        console.log('stick x', { x: position / 32767, y: cur.y })
      }
      if (this.y === byte8) {
        let cur = this.get('stick')
        this.set('stick', { x: cur.x, y: position / 32767 })
        console.log('stick y', { x: cur.x, y: position / 32767 })
      }

    } else {
      // console.log('unknown type', byte7)
    }
  }
}


module.exports = Joystick