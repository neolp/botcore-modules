const { core, Node } = require('botcorejs')

const pause = (duration) => new Promise(resolve => setTimeout(resolve, duration))

const fakeacc = 1000
const backoff = (retries, delay, fn, ...args) => {
  return fn(...args).catch((err) => {
    if (retries > 1) {
      //console.log('Again ', ...args)
      return pause(delay).then(() => backoff(retries - 1, delay * 2, fn, ...args))
    } else {
      //console.log('Reject Again ', ...args)
      return Promise.reject(err)
    }
  })
}

class mot823 extends Node {
  constructor(cfg) {
    super(cfg, core)

    //this.addVar('pollInterval', { default: 1000 })
    if (!this.cfg.fake) {
      this.connector = core.getConnector(cfg.addr)
    }
    this.addVar('leftpos', { default: 0, type: 'number' })
    this.addVar('rightpos', { default: 0, type: 'number' })
    this.addVar('pos', { default: [0, 0], type: 'point2d' })
    this.addVar('leftcurrent', { default: 0, type: 'number', label: 'left current', units: 'A', min: -16, max: 16 })
    this.addVar('rightcurrent', { default: 0, type: 'number', label: 'right current', units: 'A', min: -16, max: 16 })
    this.addVar('measuretime', { default: 0, type: 'number' })
    this.addVar('bumpers', { default: 0, type: 'number' })
    this.addVar('leftdistance', { default: 0, type: 'number', label: 'left distance', units: 'mm', min: 20, max: 6000 })
    this.addVar('rithtdistance', { default: 0, type: 'number', label: 'left distance', units: 'mm', min: 20, max: 6000 })
    this.leftspeed = 0
    this.rightspeed = 0
    this.lefttargetspeed = 0
    this.righttargetspeed = 0

    this.intstate = 'idle'
    this.intpromise = undefined
    this.inttimeout = undefined

    this.addVar('pollInterval', { default: 1000, persistent: true })

    this.populate(['goto', 'setspeed', 'setpower', 'setinvpower', 'setservo', 'stat', 'read', 'pos', 'setleftpower', 'setrightpower', 'out', 'out1', 'out2'])
    //this.set('pollInterval', cfg.pollInterval || 1000)
    this.addVar('supplyvoltage', { type: 'number', label: 'input volatge', units: 'V', min: 9, max: 16 })
    this.addVar('state', { default: 'start', persistent: false })

  }

  start() {
    let intreval = this.cfg.fake ? 200 : this.get('pollInterval')
    this.poll = setInterval(async () => {
      try {
        if (this.cfg.fake) {
          // left
          let dir = Math.sign(this.lefttargetspeed - this.leftspeed)
          if (dir) {
            this.leftspeed += fakeacc * dir
            if (dir > 0 && this.leftspeed > this.lefttargetspeed) this.leftspeed = this.lefttargetspeed
            if (dir < 0 && this.leftspeed < this.lefttargetspeed) this.leftspeed = this.lefttargetspeed
          }
          let lpos = this.get('leftpos')
          lpos += this.leftspeed
          this.set('leftpos', lpos)
          // right
          dir = Math.sign(this.righttargetspeed - this.rightspeed)
          if (dir) {
            this.rightspeed += fakeacc * dir
            if (dir > 0 && this.rightspeed > this.righttargetspeed) this.rightspeed = this.righttargetspeed
            if (dir < 0 && this.rightspeed < this.righttargetspeed) this.rightspeed = this.righttargetspeed
          }
          let rpos = this.get('rightpos')
          rpos += this.rightspeed
          this.set('rightpos', rpos)
          this.set('pos', [lpos, rpos])

          this.set('leftcurrent', this.leftspeed / 100 + Math.random() / 10)
          this.set('rightcurrent', this.rightspeed / 100 + Math.random() / 10)
          this.set('measuretime', Date.now())
          this.set('bumbers', 0)
          this.set('leftdistance', 5000 + Math.round(Math.random() * 10))
          this.set('rithtdistance', 5000 + Math.round(Math.random() * 10))

          this.fakeadc()
        } else {
          let st = this.get('state')
          if (st === 'start') {
            let cfg = await this.config()
            if (cfg)
              this.set('state', 'ready')
          } else {
            await this.adc()
            let status = await this.connector.call('sp', [])
            this.processstatus(status)
            if (this.intstate === 'moveto') { // process data
              if (Math.abs(status[1] - this.inttargetl) < 10 && Math.abs(status[2] - this.inttargetr) < 10 && Math.abs(status[3]) < 10 && Math.abs(status[4]) < 10) { // leftpos rightpos leftspeed rightspeed
                this.intstate = 'idle'
                this.intpromise.resolve()
                this.intpromise = undefined
                clearTimeout(this.inttimeout)
                this.inttimeout = undefined
              }
            }
          }
        }
      } catch (err) {
        this.set('state', 'error')
        this.set('statedesc', err.message)
      }
    }, intreval)
  }


  async connect(cnt = 5) {
    if (cnt <= 0) return // Promise.reject(new Error('try out'))
    try {
      // await this.connector.connect(this.addr, '', null)
      let description = await backoff(5, 500, this.connector.describe.bind(this.connector), this.addr, '')
      this.description = description
      this.ready = true
      if (Array.isArray(description)) {
        this.description = description[0]
        description.shift()
        this.links = []
        for (let lnk of description) {
          this.links.push(lnk)
        }
      }
      //console.log('Connected ', this.addr, this.description, this.links)
    } catch (err) {
      // setTimeout(this.connect.bind(this, cnt - 1), 1000)
      //console.log('connect error')
    }
  }
  async config() {
    //let dt = await this.readstat()
    let description = await this.connector.describe('')
    console.log(description)
    this.description = description
    this.ons = await this.connector.call('cfg', [1, 1, 1]) // global on
    console.log('cfg ret',this.ons)
    return true
    //console.log('Connected ', this.addr, this.description, this.links)
  }

  processstatus(data) {
    if (Array.isArray(data)) {
      if (data.length >= 10) {
        //console.log(data)
        let st = (data[0] >> 2) & 0x3F
        if (st != 0) this.set('state', 'start')
        this.set('bumpers', (data[0] & 0x03)^0x03)
        this.set('inputs', (data[0] >> 8) & 0x0F)
        this.set('leftpos', data[1])
        this.set('rightpos', data[2])
        this.set('pos', [data[1], data[2]])
        this.set('leftspeed', data[3])
        this.set('rightspeed', data[4])
        this.set('leftmspeed', data[5])
        this.set('rightmspeed', data[6])
        this.set('leftpwm', data[7])
        this.set('rightpwm', data[8])

        this.set('leftcurrent', data[9])
        this.set('rightcurrent', data[10])
        this.set('measuretime', data[11])
      }
      if (data.length >= 14) {
        this.set('leftdistance', data[12])
        this.set('rithtdistance', data[13])
      }
    }
  }

  async gotoA(left, right, speed, acc) {
    let param = [...arguments]
    return new Promise(async (resolve, reject) => {
      this.intstate = 'moveto'
      await this.connector.call('to', param)
      this.intpromise = { resolve, reject }
      this.inttimeout = setTimeout(() => {
        reject("timeout for move")
        this.intpromise = undefined
        this.intstate = 'idle'
        this.inttimeout = undefined
      }, 1000)
    })
  }
  goto(left, right, speed, acc) {
    let param
    switch (arguments.length) {
      case 2:
        param = [+left, +right]
        break
      case 3:
        param = [+left, +right, speed]
        break
      case 4:
        param = [+left, +right, speed, acc]
        break
      default:
        throw new Error('illegal argument count')
    }
    const that = this
    return this.connector.call('to', param).then((data) => {
      return that.processstatus(data)
    })
  }
  async setspeed(left, right, acc) {
    if (this.cfg.fake) {
      this.lefttargetspeed = left
      this.righttargetspeed = right
    } else {
      let param
      switch (arguments.length) {
        case 2:
          param = [Math.round(left), Math.round(right)]
          break
        case 3:
          param = [Math.round(left), Math.round(right), Math.round(acc)]
          break
        default:
          throw new Error('illegal argument count')
      }
      return this.connector.call('sp', param).then((data) => {
        return this.processstatus(data)
      })
    }
  }

  setpower(left, right) {
    const that = this
    return this.connector.call('go', [left, right]).then((data) => {
      return that.processstatus(data)
    })
  }

  setpid(K, P, I, D) {
    return this.connector.call('pid', [K, P, I, D])
  }

  setservo(first, second) {
    return this.connector.call('servo', [first, second])
  }

  async adc() {
    return this.connector.call('adc', null).then((data) => {
      if (Array.isArray(data)) {
        let st = data[0] >> 2
        if (st != 0) this.set('state', 'start')
        this.set('bumbers', data[0] & 0x03)
        this.set('supplyvoltage', data[1] / 1000)
        this.set('temperature', data[2])
        this.set('in1', data[3])
        this.set('in2', data[4])
        this.set('cs1', data[5])
        this.set('cs2', data[6])
        this.set('extT', data[7])
        this.set('intV', data[8] * 3.3 / 4095)
      }
      return data
    }).catch(err => {
      this.set('state', 'error')
      this.set('statedesc', err.message)
    })
  }

  fakeadc() {
    this.set('supplyvoltage', 12 + Math.round(Math.random() * 20) / 10)
    this.set('temperature', 25 + Math.round(Math.random() * 2))
    this.set('in1', 0)
    this.set('in2', 1)
    this.set('in3', 2)
    this.set('in4', 3)
    this.set('cs1', 2 + Math.round(Math.random() * 20) / 10)
    this.set('cs2', 2 + Math.round(Math.random() * 20) / 10)
  }

  out(_out1, _out2) {
    this._out1 = _out1
    this._out2 = _out2
    return this.connector.call('out', [Math.round(_out1), Math.round(_out2)]).then(
      (data) => {
        if (Array.isArray(data)) {
          this.set('_out1', data[0])
          this.set('_out2', data[1])
        }
        return data
      })
  }

  async out1(_out1) {
    try {
      let ret = await this.out(_out1, this._out2)
      return ret
    } catch (ex) {
      return null
    }
  }
  async out2(_out2) {
    try {
      let ret = await this.out(this._out1, _out2)
      return ret
    } catch (ex) {
      return null
    }
  }
  read() {
    let data = this.stat()
    return data
  }
  /*  pos() {
      const that = this
      return this.connector.call('to', null).then((data) => {
        return that.processstatus(data)
      })
    }*/
}

/*
const EventEmitter = require('events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
myEmitter.on('event', () => {
  console.log('an event occurred!');
});
myEmitter.emit('event');
*/

module.exports = mot823
