const os = require('os')
const { Node, core } = require('botcorejs')

class Rpisensors extends Node {
  constructor(cfg) {
    super(cfg, core)
    this.cpus = os.cpus()
    this.addVar('pollInterval', { default: 1000, persistent: true })
    this.addVar('temp', { type: 'number', label: 'temperature', units: '°C', min: 0, max: 80 })
  }

  start() {
    let intreval = this.get('pollInterval')
    this.poll = setInterval(() => {
      let cpus = os.cpus()
      let totidle = 0
      let totwork = 0
      let syscpus = []
      for (let i = 0; i < cpus.length; i++) {
        let idle = (cpus[i].times.idle - this.cpus[i].times.idle) >>> 0
        let work = (cpus[i].times.user - this.cpus[i].times.user) >>> 0
        work += (cpus[i].times.sys - this.cpus[i].times.sys) >>> 0
        work += (cpus[i].times.irq - this.cpus[i].times.irq) >>> 0
        syscpus.push(work / (work + idle))
        totidle += idle
        totwork += work
      }
      this.cpus = cpus
      this.set('cpus', syscpus)
      this.set('cpu', totwork / (totwork + totidle))
      this.set('freemem', os.freemem())
      this.set('totalmem', os.totalmem())
      this.set('uptime', os.uptime())
      this.set('hostname', os.hostname())
    }, intreval)
  }
}



module.exports = Rpisensors
