const net = require('net');
const fs = require('fs')
const spawn = require('child_process').spawn;
//const Splitter = require('stream-split');
const { Node, core } = require('botcorejs')
var cpudetect = require('botcorejs/cpudetect')()
//const Throttle = require('stream-throttle').Throttle;

const Transform = require('stream').Transform;

//const NALseparator = new Buffer([0, 0, 0, 1]);//NAL break
const NALseparator = Buffer.from([0, 0, 0, 1]);//NAL break
/*
class Splitter extends Transform {
  constructor(separator, options) {
    if (!options)
      options = {};
    super(options);

    this.offset = 0;
    this.bodyOffset = 0;

    this.bufferSize = options.bufferSize || 1024 * 1024 * 1; //1Mb
    this.bufferFlush = options.bufferFlush || Math.floor(this.bufferSize * 0.1); //10% buffer size

    this.buffer = Buffer.allocUnsafe(this.bufferSize); this.buffer.fill(0);
    this.separator = separator;
  }

  _transform(chunk, encoding, next) {

    if (this.offset + chunk.length > this.bufferSize - this.bufferFlush) {
      var minimalLength = this.bufferSize - this.bodyOffset + chunk.length;
      if (this.bufferSize < minimalLength) {
        //console.warn("Increasing buffer size to ", minimalLength);
        this.bufferSize = minimalLength;
      }

      var tmp = new Buffer(this.bufferSize);
      this.buffer.copy(tmp, 0, this.bodyOffset);
      this.buffer = tmp;
      this.offset = this.offset - this.bodyOffset;
      this.bodyOffset = 0;
    }

    chunk.copy(this.buffer, this.offset);

    var i, start, stop = this.offset + chunk.length;
    do {
      start = Math.max(this.bodyOffset ? this.bodyOffset : 0, this.offset - this.separator.length);
      i = this.buffer.slice(start, stop).indexOf(this.separator);

      if (i == -1)
        break;

      i += start;
      var img = this.buffer.slice(this.bodyOffset, i + this.separator.length); // img including separator
      this.push(img);
      this.bodyOffset = i + this.separator.length;
    } while (true);

    this.offset += chunk.length;
    next();
  }
};
*/

class Splitter extends Transform {

  constructor(separator, options) {
    if (!options)
      options = {};
    super(options);

    this.offset = 0;
    this.bodyOffset = 0;

    this.bufferSize = options.bufferSize || 1024 * 1024 * 1; //1Mb
    this.bufferFlush = options.bufferFlush || Math.floor(this.bufferSize * 0.1); //10% buffer size

    //this.buffer = new Buffer(this.bufferSize); this.buffer.fill(0);
    this.buffer = Buffer.alloc(this.bufferSize, 0);
    this.separator = separator;
  }

  _transform(chunk, encoding, next) {

    if (this.offset + chunk.length > this.bufferSize - this.bufferFlush) {
      var minimalLength = this.bufferSize - this.bodyOffset + chunk.length;
      if (this.bufferSize < minimalLength) {
        //console.warn("Increasing buffer size to ", minimalLength);
        this.bufferSize = minimalLength;
      }

      //var tmp = new Buffer(this.bufferSize);
      let tmp = Buffer.alloc(this.bufferSize);
      this.buffer.copy(tmp, 0, this.bodyOffset);
      this.buffer = tmp;
      this.offset = this.offset - this.bodyOffset;
      this.bodyOffset = 0;
    }

    chunk.copy(this.buffer, this.offset);

    var i, start, stop = this.offset + chunk.length;
    do {
      start = Math.max(this.bodyOffset ? this.bodyOffset : 0, this.offset - this.separator.length);
      i = this.buffer.slice(start, stop).indexOf(this.separator);

      if (i == -1)
        break;

      i += start;
      var img = this.buffer.slice(this.bodyOffset, i);
      this.push(img);
      this.bodyOffset = i + this.separator.length;
    } while (true);

    this.offset += chunk.length;
    next();
  }
};

class picamera extends Node {
  constructor(cfg) {
    super(cfg, core)
    this.startting = true
    //this.cfg = {}
    if (!this.cfg.fps) this.cfg.fps = 10
    if (!this.cfg.width) this.cfg.width = 640
    if (!this.cfg.height) this.cfg.height = 480

    this.findffmpegcameras()
  }

  start() {
    //this.startRecord('timelapse', './video/', '.jpg', 0, 20)
    //this.startRecord('', './video/', '.avi', 2 * 60 * 1000, 1)
    this.start_stream()
  }

  start_stream() {


    const server = net.createServer((c) => {
      // 'connection' listener.
      console.log('ffmpeg connected');
      c.on('end', () => {
        console.log('ffmpeg disconnected');
      });
      c.pipe(new Splitter(NALseparator)).on('data', (dataunit) => {
        this.set('unit', Buffer.concat([NALseparator, dataunit])) // set unit one by one and all of this will be sended to player
      })
    });
    server.on('error', (err) => {
      throw err;
    });
    server.listen(8124, () => {
      console.log('server bound');
    });

    //    server.listen(0, function () {
    //      console.log('Listening on port ' + srv.address().port);
    //    });

    var argss = [
      "-f", "gdigrab",
      "-framerate", this.cfg.fps,
      "-offset_x", 10,
      "-offset_y", 20,
      "-video_size", this.cfg.width + 'x' + this.cfg.height,
      '-i', 'desktop',
      '-pix_fmt', 'yuv420p',
      '-c:v', 'libx264',
      '-vprofile', 'baseline',
      '-tune', 'zerolatency',
      '-f', 'rawvideo',
      '-'
    ];

    //https://trac.ffmpeg.org/wiki/Limiting%20the%20output%20bitrate
    //ffmpeg -list_options true -f dshow -i video="USB2.0 HD UVC AF Camera"
    //ffmpeg -list_options true -f dshow -i video="USB2.0 HD UVC AF Camera"
    var argsw = [
      "-video_size", this.cfg.width + 'x' + this.cfg.height,
      "-framerate", this.cfg.fps,
      "-f", "dshow",
      //"-i", 'video=USB2.0 HD UVC AF Camera',
      //      "-i", 'video=USB-видеоустройство',
      "-i", 'video=Integrated Camera',
      //"-i", 'video="USB2.0 HD UVC AF Camera"',
      // "-video_device_index", "1",
      // "-i", 'video="0"',
      // '-pixel_format', 'yuyv422',
      '-pix_fmt', 'yuv420p',
      '-c:v', 'libx264',
      '-b:v', '600k',
      '-bufsize', '600k',
      '-vprofile', 'baseline',
      '-tune', 'zerolatency',
      '-f', 'rawvideo',
      '-'
      //'tcp://127.0.0.1:8124'
    ];

    this.cfg.fps = 12
    this.cfg.width = 960
    this.cfg.height = 540

    var argsraspivid = ['-t', '0', '-o', '-', '--segment', 3000, '-w', this.cfg.width, '-h', this.cfg.height, '-fps', this.cfg.fps, '-pf', 'baseline']

    console.info(argsw.join(' '))
    //cpudetect = {}
    if (cpudetect.isWin) {
      this.streamer = spawn('ffmpeg', argsw)
    } else if (cpudetect.isPi) {
      this.streamer = spawn('raspivid', argsraspivid)
    } else {
      // var readStream = fs.createReadStream('./samples/admiral.264');
      // this.streamer = readStream.pipe(new Throttle({ rate: 100000 }));
      // this.streamer.pipe(new Splitter(NALseparator)).on('data', (dataunit) => {
      //   this.set('unit', Buffer.concat([NALseparator, dataunit])) // set unit one by one and all of this will be sended to player
      //   console.log('+', dataunit[0])
      // })
      // this.streamer.on('end', () => {
      //   console.log('end')
      // })
    }
    this.streamer.on("exit", function (code) {
      console.log("Failure", code);
    });
    this.streamer.stdout.pipe(new Splitter(NALseparator)).on('data', (dataunit) => {
      this.set('unit', Buffer.concat([NALseparator, dataunit])) // set unit one by one and all of this will be sended to player
      ///console.log('+')
    })
    this.streamer.stderr.on('data', (data) => {
      //console.error(`stderr: ${data}`);
    });


  }
  findffmpegcameras() {
    return new Promise((resolve, reject) => {
      const argsw = [
        "-list_devices", "true",
        "-f", "dshow",
        "-i", "dummy"
      ];
      const argsl = [
        "-list_devices", "true",
        "-f", "dshow",
        "-i", "dummy"
      ];

      let stream
      let instring = ""
      if (cpudetect.isWin) {
        stream = spawn('ffmpeg', argsw)
      } else if (cpudetect.isPi) {
        stream = spawn('ffmpeg', argsl)
      } else {
      }
      stream.on("close", function (code) {
        const reg = /(\[.*\]\s+"(.*)".*)|(\[.*\]\s+DirectShow (\S+))/gm
        let res
        let invideos = false
        let found = []
        while (res = reg.exec(instring)) {
          if (res[4] === 'video') invideos = true
          else if (invideos && res[2]) found.push(res[2])
          else if (res[4]) break
        }
        console.log('found', found)
        found.sort()

        if (instring.length > 0) {
          resolve(found)
        }
        reject(code)
      });
      stream.stdout.on('data', (data) => { instring += data })
      stream.on('error', (data) => { reject(data) })
      stream.stderr.on('data', (data) => { instring += data });
    })
  }

  onsubscribe(trigger) {
    if (trigger === 'image') {
      if (this.records.size == 0) {
        this.StartCapture()
      }
      this.subscribed = true
    }
  }
  onunsubscribe(trigger) {
    if (trigger === 'image') {
      this.subscribed = false
      if (this.records.size == 0) {
        this.StopCapture()
      }
    }
  }

}
module.exports = picamera
