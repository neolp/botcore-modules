const { core, Node } = require('botcorejs')
class RMD extends Node {
  constructor(cfg) {
    super(cfg, core)
    this.connector = core.getConnector(cfg.addr)
  }

  readstat() {
    return this.connector.call('stat', []).then((data) => {
      console.log('data', data)
      return data
    })
  }
  ledset(on) {
    return this.connector.call('led', [on ? 1 : 0]).then((data) => {
      console.log('led', data)
      return data
    })
  }
  ledread() {
    return this.connector.call('led', []).then((data) => {
      console.log('led', data)
      return data
    })
  }
  home(sp, acc) {
    return this.connector.call('home', [sp, acc]).then((data) => {
      console.log('home', data)
      return data
    })
  }
  to(pos, sp, acc) {
    return this.connector.call('to', [pos, sp, acc]).then((data) => {
      console.log('to', data)
      return data
    })
  }
}
module.exports = RMD