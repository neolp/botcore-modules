const os = require('os')
const fs = require('fs')
var vcgencmd = require('vcgencmdjs');
const { Node, core } = require('botcorejs')

class Rpisensors extends Node {
  constructor(cfg) {
    super(cfg, core)
    this.cpus = os.cpus()
    this.addVar('pollInterval', { default: 1000, persistent: true })
    this.addVar('temp', { type: 'number', label: 'temperature', units: '°C', min: 0, max: 80 })
    this.netstat = this.readnet()
    this.netstattime = Date.now()
  }

  readnet() {
    let data = fs.readFileSync('/proc/net/dev', { encoding: 'utf8' })
    let strings = data.split('\n')
    let vals = strings.map(str => str.split(/[\:\s]+/))
    let netstat = {}
    for (let i = 2; i < vals.length - 1; i++) {
      if (vals[i][1] != 'lo:') {
        let s = { rbytes: vals[i][2], rpackets: vals[i][3], tbytes: vals[i][10], tpackets: vals[i][11] }
        netstat[vals[i][1]] = s
      }
    }
    return netstat
  }

  pollnet() {
    let now = Date.now()
    let netstat = this.readnet()
    let netvals = {}
    for (const net in netstat) {
      let s = netstat[net]
      let p = this.netstat[net]
      let v = {}
      v.rxbytes = ((s.rbytes - p.rbytes) >>> 0) * 1000.0 / (now - this.netstattime)
      v.rxpkts = ((s.rpackets - p.rpackets) >>> 0) * 1000.0 / (now - this.netstattime)
      v.txbytes = ((s.tbytes - p.tbytes) >>> 0) * 1000.0 / (now - this.netstattime)
      v.txpkts = ((s.tpackets - p.tpackets) >>> 0) * 1000.0 / (now - this.netstattime)
      netvals[net] = v
    }
    this.set('net', netvals)

    this.netstattime = now
    this.netstat = netstat
  }

  start() {
    let intreval = this.get('pollInterval')
    this.poll = setInterval(() => {
      let cpus = os.cpus()
      let totidle = 0
      let totwork = 0
      let syscpus = []
      for (let i = 0; i < cpus.length; i++) {
        let idle = (cpus[i].times.idle - this.cpus[i].times.idle) >>> 0
        let work = (cpus[i].times.user - this.cpus[i].times.user) >>> 0
        work += (cpus[i].times.sys - this.cpus[i].times.sys) >>> 0
        work += (cpus[i].times.irq - this.cpus[i].times.irq) >>> 0
        syscpus.push(work / (work + idle))
        totidle += idle
        totwork += work
      }
      this.cpus = cpus
      this.set('cpus', syscpus)
      this.set('cpu', totwork / (totwork + totidle))
      this.set('freemem', os.freemem())
      this.set('totalmem', os.totalmem())
      this.set('uptime', os.uptime())
      this.set('hostname', os.hostname())
      try {
        // cat /sys/class/thermal/thermal_zone0/temp
        this.set('thtottled', [...vcgencmd.throttled()])
        this.set('temp', vcgencmd.measureTemp())
        this.set('clock', vcgencmd.measureClock('arm'))
        //arm	ARM cores
        //core	VC4 scaler cores
        //H264	H264 block
        //isp	Image Signal Processor
        //v3d	3D block
        //uart	UART
        //pwm	PWM block(analogue audio output)
        //emmc	SD card interface
        //pixel	Pixel valve
        //vec	Analogue video encoder
        //hdmi	HDMI
        //dpi	Display Peripheral Interface
      } catch (err) {
        this.set('temp', NaN)
        this.set('clock', NaN)
      }
      this.pollnet()
    }, intreval)
  }
}



module.exports = Rpisensors
