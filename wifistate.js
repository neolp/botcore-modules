const { exec } = require('child_process');
const { Node, core } = require('botcorejs')

const channlfreq = {
  1: 2.412,
  2: 2.417,
  3: 2.422,
  4: 2.427,
  5: 2.432,
  6: 2.437,
  7: 2.442,
  8: 2.447,
  9: 2.452,
  10: 2.457,
  11: 2.462,
  12: 2.467,
  13: 2.472,
  14: 2.484,
  /*
    131: 3.6575,
    132: 3.66225,
    132: 3.6600,
    133: 3.6675,
    133: 3.6650,
    134: 3.6725,
    134: 3.6700,
    135: 3.6775,
    136: 3.6825,
    136: 3.6800,
    137: 3.6875,
    137: 3.6850,
    138: 3.6895,
    138: 3.6900,
  */
  36: 5.180,
  40: 5.200,
  44: 5.220,
  48: 5.240,
  52: 5.260,
  56: 5.280,
  60: 5.300,
  64: 5.320,
  100: 5.500,
  104: 5.520,
  108: 5.540,
  112: 5.560,
  116: 5.580,
  120: 5.600,
  124: 5.620,
  128: 5.640,
  132: 5.660,
  136: 5.680,
  140: 5.700,
  149: 5.745,
  153: 5.765,
  157: 5.785,
  161: 5.805,
  165: 5.825,
}

const iwconfigregexp = /SSID:"(?<ssid>[^"]*)"[\S\s]*Frequency:(?<freq>[\d\.]+)[\S\s]*Bit Rate=(?<rate>[\d\.]+)[\S\s]*Quality=(?<quality>[\d\.]+)\/(?<of>[\d\.]+)[\S\s]*level=(?<level>[-\d\.]+)/
const netshregexp = /SSID\s*: (?<ssid>[^\n]*)[\S\s]*Channel\s*: (?<channel>[\d\.]+)[\S\s]*Receive rate[^:]*: (?<rate>[\d\.]+)[\S\s]*Transmit rate[^:]*: (?<trate>[\d\.]+)[\S\s]*Signal\s*: (?<quality>[\d\.]+)/
class Wifistate extends Node {
  constructor(cfg) {
    super(cfg, core)
    this.addVar('pollInterval', { default: 1000, persistent: true })
    this.addVar('temp', { type: 'number', label: 'temperature', units: '°C', min: 0, max: 80 })
  }

  start() {
    let intreval = this.get('pollInterval')
    switch (process.platform) {
      case 'linux':
        this.poll = setInterval(() => { this.get_linux() }, intreval)
        break;
      case 'win32':
        this.poll = setInterval(() => { this.get_win32() }, intreval)
        break;
      default:
        throw new Error('ERROR : UNRECOGNIZED OS');
    }
  }
  get_linux() {
    exec('iwconfig wlan0', (error, stdout, stderr) => {
      if (error) {
        this.set('temp', NaN)
        this.set('clock', NaN)
        return;
      }
      let parse = stdout.match(iwconfigregexp)
      if (!parse || !parse.groups) {
        return undefined
        //          console.log(`stdout: ${stdout}`);
        //          console.error(`stderr: ${stderr}`);
      }
      this.set('ssid', parse.groups.ssid) // Group`ssid`	n / a	AlexN
      this.set('freq', +parse.groups.freq) // Group`freq`	n / a	2.412
      this.set('rate', +parse.groups.rate) // Group`rate`	n / a	72.2
      this.set('quality', +parse.groups.quality / +parse.groups.of) // Group`quality`	n / a	54
      //      this.set('of', +parse.groups.of) // Group`of`	n / a	70
      this.set('level', +parse.groups.level) // Group`level`	n / a - 56
    });
  }

  get_win32() {
    exec('netsh wlan show interfaces', (error, stdout, stderr) => {
      if (error) {
        this.set('temp', NaN)
        this.set('clock', NaN)
        return;
      }
      let parse = stdout.match(netshregexp)
      if (!parse || !parse.groups) {
        return undefined
        //          console.log(`stdout: ${stdout}`);
        //          console.error(`stderr: ${stderr}`);
      }
      this.set('ssid', parse.groups.ssid) // Group`ssid`	n / a	AlexN
      this.set('freq', channlfreq[parse.groups.channel]) // Group`freq`	n / a	2.412
      this.set('rate', +parse.groups.rate) // Group`rate`	n / a	72.2
      this.set('quality', +parse.groups.quality / 100) // Group`quality`	n / a	54
      //      this.set('of', +parse.groups.of) // Group`of`	n / a	70
      this.set('level', +parse.groups.level) // Group`level`	n / a - 56
    });
  }
}

// cat /proc/net/dev
module.exports = Wifistate
